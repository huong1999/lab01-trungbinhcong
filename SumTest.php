<?php
require_once 'Sum.php';

use PHPUnit\Framework\TestCase;

class SumTest extends TestCase
{
    //Testcase 1: tổng của n và m chính xác
    function test_sum_of_n_and_m()
    {
        $this->assertEquals(2, Sum(1, 1));
    }
}
